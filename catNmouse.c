///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.1 - Stopped Seg Fault when not entering an argument
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Leo Liang <leoliang@hawaii.edu>
/// @date    1/2/2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   
   
   int DEFAULT_MAX_NUMBER = 2048;
   int MAX_VALUE = DEFAULT_MAX_NUMBER;
   int USER_MAX = DEFAULT_MAX_NUMBER;
   int Correct = 13;
   if(argc == 2)
   {
      int USER_MAX = atoi(argv[1]);
      if(USER_MAX < 1 )
      {
         printf("this is here");
         return 1;
      }
      else
      {
         MAX_VALUE = USER_MAX;
         Correct = ((rand() % MAX_VALUE) + 1);
      }
   }
   else
   {
    Correct=((rand() % MAX_VALUE) + 1);
   }
   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5. 
   
   
   do {
   int aGuess;
   printf( "OK cat, I'm thinking of a number from 1 to %d.    Make a guess:      ", MAX_VALUE );
   scanf( "%d", &aGuess );
   if(aGuess < 1)
   {
   printf( "You must enter a number that's >=1. \n");
    }
    else if(aGuess > MAX_VALUE)
    {
       printf( "You must enter a number that's <= %d. \n" ,MAX_VALUE);
    }
       else if(aGuess < Correct)
   {
   printf( "No cat... the number I'm thinking of is larger than %d. \n", aGuess);
   }
   else if(aGuess > Correct)
   {
      printf("No cat... the number I'm thinking of is less than %d. \n", aGuess);
   }
   else if(aGuess == Correct)
   {
      printf("You got me.\n");
         printf(" /\\_/\\ \n");
printf("( o.o )\n" );
printf( " > ^ < \n");
         return 0;
}
} while (1);
}

